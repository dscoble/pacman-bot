import random
from pacai.agents.base import BaseAgent
from pacai.agents.search.multiagent import MultiAgentSearchAgent
from pacai.core.directions import Directions

class ReflexAgent(BaseAgent):
    """
    A reflex agent chooses an action at each choice point by examining
    its alternatives via a state evaluation function.

    The code below is provided as a guide.
    You are welcome to change it in any way you see fit,
    so long as you don't touch the method headers.
    """

    def __init__(self, index, **kwargs):
        super().__init__(index)

    def getAction(self, gameState):
        """
        You do not need to change this method, but you're welcome to.

        `ReflexAgent.getAction` chooses among the best options according to the evaluation function.

        Just like in the previous project, this method takes a
        `pacai.core.gamestate.AbstractGameState` and returns some value from
        `pacai.core.directions.Directions`.
        """

        # Collect legal moves.
        legalMoves = gameState.getLegalActions()

        # Choose one of the best actions.
        scores = [self.evaluationFunction(gameState, action) for action in legalMoves]
        bestScore = max(scores)
        bestIndices = [index for index in range(len(scores)) if scores[index] == bestScore]
        chosenIndex = random.choice(bestIndices)  # Pick randomly among the best.

        return legalMoves[chosenIndex]

    def evaluationFunction(self, currentGameState, action):
        """
        Design a better evaluation function here.

        The evaluation function takes in the current `pacai.bin.pacman.PacmanGameState`
        and an action, and returns a number, where hig;her numbers are better.
        Make sure to understand the range of different values before you combine them
        in your evaluation function.
        """

        successorGameState = currentGameState.generatePacmanSuccessor(action)

        # Useful information you can extract.
        newPosition = successorGameState.getPacmanPosition()
        food = successorGameState.getFood().asList()
        newGhostStates = successorGameState.getGhostStates()
        danger = 0
        minDistance = 1
        # *** Your Code Here ***

        # find smallest distance to each food from pacman
        distances = []
        for pellet in food:
            distances.append(abs(newPosition[0] - pellet[0]) + abs(newPosition[1] - pellet[1]))
        if len(distances) > 0:
            minDistance = min(distances)
        # quantify the amount of danger pacman is in
        for ghostState in newGhostStates:
            ghost = ghostState.getPosition()
            # distance = 0 if manhattan distance is > 4
            # else distance = 4 - manhattan
            dist = max(4 - (abs(newPosition[0] - ghost[0]) + abs(newPosition[1] - ghost[1])), 0)
            danger += dist * dist
        # combine distance to food and distance to ghosts
        return successorGameState.getScore() + 10 / minDistance - danger

class MinimaxAgent(MultiAgentSearchAgent):
    """
    A minimax agent.

    Here are some method calls that might be useful when implementing minimax.

    `pacai.core.gamestate.AbstractGameState.getNumAgents()`:
    Get the total number of agents in the game

    `pacai.core.gamestate.AbstractGameState.getLegalActions`:
    Returns a list of legal actions for an agent.
    Pacman is always at index 0, and ghosts are >= 1.

    `pacai.core.gamestate.AbstractGameState.generateSuccessor`:
    Get the successor game state after an agent takes an action.

    `pacai.core.directions.Directions.STOP`:
    The stop direction, which is always legal, but you may not want to include in your search.

    Method to Implement:

    `pacai.agents.base.BaseAgent.getAction`:
    Returns the minimax action from the current gameState using
    `pacai.agents.search.multiagent.MultiAgentSearchAgent.getTreeDepth`
    and `pacai.agents.search.multiagent.MultiAgentSearchAgent.getEvaluationFunction`.
    """

    def __init__(self, index, **kwargs):
        self.args = kwargs
        super().__init__(index)

    def getAction(self, state):
        scoreMax = []
        scoreMin = []
        # if the game is over, do not return an action
        if state.isOver():
            return Directions.STOP
        for agentID in range(0, state.getNumAgents()):
            # get the legal actions of each agent, and remove 'stop'
            # because the agents should always be moving
            legalActions = state.getLegalActions(agentID)
            if 'Stop' in legalActions:
                legalActions.remove('Stop')
            # for pacman, maximize:
            if agentID == 0:
                for action in legalActions:
                    scoreMax.append(self.getEvaluationFunction()(state.generateSuccessor
                        (agentID, action)))
                return legalActions[scoreMax.index(max(scoreMax))]
            # for ghosts, minimize:
            else:
                for action in legalActions:
                    scoreMin.append(self.getEvaluationFunction()(state.generateSuccessor
                        (agentID, action)))
                return legalActions[scoreMin.index(min(scoreMin))]
        return Directions.STOP

class AlphaBetaAgent(MultiAgentSearchAgent):
    """
    A minimax agent with alpha-beta pruning.

    Method to Implement:

    `pacai.agents.base.BaseAgent.getAction`:
    Returns the minimax action from the current gameState using
    `pacai.agents.search.multiagent.MultiAgentSearchAgent.getTreeDepth`
    and `pacai.agents.search.multiagent.MultiAgentSearchAgent.getEvaluationFunction`.
    """

    def __init__(self, index, **kwargs):
        super().__init__(index)

    def getAction(self, state):
        scoreMax = []
        scoreMin = []
        # if the game is over, do not return an action
        if state.isOver():
            return Directions.STOP
        for agentID in range(0, state.getNumAgents()):
            # get the legal actions of each agent, and remove 'stop'
            # because the agents should always be moving
            legalActions = state.getLegalActions(agentID)
            if 'Stop' in legalActions:
                legalActions.remove('Stop')
            # for pacman, maximize:
            if agentID == 0:
                for action in legalActions:
                    curScore = self.getEvaluationFunction()(state.generateSuccessor
                        (agentID, action))
                    # if we evaluate a score greater than the max, ignore the
                    # rest of that action
                    if len(scoreMin) == 0 or curScore > max(scoreMax):
                        scoreMax.append(curScore)
                    else:
                        break
                if len(scoreMax) > 0:
                    return legalActions[scoreMax.index(max(scoreMax))]
            # for ghosts, minimize:
            else:
                for action in legalActions:
                    curScore = self.getEvaluationFunction()(state.generateSuccessor
                        (agentID, action))
                    # if we evaluate a score less than the min, ignore the
                    # rest of that action
                    if curScore <= min(scoreMin) or len(scoreMin) == 0:
                        scoreMin.append(curScore)
                    else:
                        break
                if len(scoreMin) > 0:
                    return legalActions[scoreMin.index(min(scoreMin))]
        return Directions.STOP

class ExpectimaxAgent(MultiAgentSearchAgent):
    """
    An expectimax agent.

    All ghosts should be modeled as choosing uniformly at random from their legal moves.

    Method to Implement:

    `pacai.agents.base.BaseAgent.getAction`:
    Returns the expectimax action from the current gameState using
    `pacai.agents.search.multiagent.MultiAgentSearchAgent.getTreeDepth`
    and `pacai.agents.search.multiagent.MultiAgentSearchAgent.getEvaluationFunction`.
    """
    def __init__(self, index, **kwargs):
        super().__init__(index, **kwargs)

    def getAction(self, state):
        scoreMax = []
        # if the game is over, do not move anymore
        if state.isOver():
            return Directions.STOP
        for agentID in range(0, state.getNumAgents()):
            # get all legal actions for agent, remove the stop action
            # because all agents are always moving
            legalActions = state.getLegalActions(agentID)
            if 'Stop' in legalActions:
                legalActions.remove('Stop')
            # for pacman:
            if agentID == 0:
                for action in legalActions:
                    scoreMax.append(self.getEvaluationFunction()(state.generateSuccessor
                        (agentID, action)))
                return legalActions[scoreMax.index(max(scoreMax))]
            # for ghosts, assume their action is random
            else:
                return legalActions[random.randint(0, len(legalActions) - 1)]
        return Directions.STOP

def betterEvaluationFunction(currentGameState):
    """
    Your extreme ghost-hunting, pellet-nabbing, food-gobbling, unstoppable evaluation function.

    DESCRIPTION: I simply wrote an evaluation function that decides if pacman is in danger
    (within 4 spaces of a ghost), he will prioritize getting out of danger. Else, he will
    prioritize getting pellets.
    """
    pacmanPos = currentGameState.getPacmanPosition()
    ghostStates = currentGameState.getGhostStates()
    food = currentGameState.getFood().asList()
    danger = 0
    distances = []
    minDistance = 1
    # check each ghost to see if they are within 4 spaces of pacman
    for ghost in ghostStates:
        ghostPos = ghost.getPosition()
        if (abs(pacmanPos[0] - ghostPos[0]) + abs(pacmanPos[1] - ghostPos[1])) > 4:
            # if pacman is not in danger, get the distance to the nearest food
            for pellet in food:
                distances.append(abs(pacmanPos[0] - pellet[0]) + abs(pacmanPos[1] - pellet[1]))
            if len(distances) > 0:
                minDistance = min(distances)
        else:
            # if pacman is in danger
            # distance = 0 if manhattan distance is > 4
            # else distance = 4 - manhattan
            dist = max(4 - (abs(pacmanPos[0] - ghostPos[0]) + abs(pacmanPos[1] - ghostPos[1])), 0)
            danger += dist * dist
    return currentGameState.getScore() + 10 / minDistance - danger

class ContestAgent(MultiAgentSearchAgent):
    """
    Your agent for the mini-contest.

    You can use any method you want and search to any depth you want.
    Just remember that the mini-contest is timed, so you have to trade off speed and computation.

    Ghosts don't behave randomly anymore, but they aren't perfect either -- they'll usually
    just make a beeline straight towards Pacman (or away if they're scared!)

    Method to Implement:

    `pacai.agents.base.BaseAgent.getAction`
    """

    def __init__(self, index, **kwargs):
        super().__init__(index)
