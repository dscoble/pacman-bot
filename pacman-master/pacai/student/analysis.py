"""
Analysis question.
Change these default values to obtain the specified policies through value iteration.
If any question is not possible, return just the constant NOT_POSSIBLE:
```
return NOT_POSSIBLE
```
"""

NOT_POSSIBLE = None

def question2():
    """
    I just changed the noise to 0
    """

    answerDiscount = 0.9
    answerNoise = 0.0

    return answerDiscount, answerNoise

def question3a():
    """
    I took out the noise factor and reduced
    the discount to   0.1
    """

    answerDiscount = 0.1
    answerNoise = 0.0
    answerLivingReward = 0.0

    return answerDiscount, answerNoise, answerLivingReward

def question3b():
    """
    I could not figure this one out
    """

    answerDiscount = 0.1
    answerNoise = 0.5
    answerLivingReward = 0.8

    return answerDiscount, answerNoise, answerLivingReward

def question3c():
    """
    I increased the discount, removed the noise, and added
    a penalty of 0.5 staying alive.
    """

    answerDiscount = 1.0
    answerNoise = 0.0
    answerLivingReward = -0.5

    return answerDiscount, answerNoise, answerLivingReward

def question3d():
    """
    I added a penalty of 0.5 for staying alive
    """

    answerDiscount = 0.9
    answerNoise = 0.2
    answerLivingReward = -0.5

    return answerDiscount, answerNoise, answerLivingReward

def question3e():
    """
    I increased the discount, removed the noise, and removed the
    penalty for staying alive, so the pacman is not penalized for not
    doing anything.
    """

    answerDiscount = 1.0
    answerNoise = 0.0
    answerLivingReward = 0.0

    return answerDiscount, answerNoise, answerLivingReward

def question6():
    """
    this question did not have any possible solutions
    """
    return NOT_POSSIBLE

if __name__ == '__main__':
    questions = [
        question2,
        question3a,
        question3b,
        question3c,
        question3d,
        question3e,
        question6,
    ]

    print('Answers to analysis questions:')
    for question in questions:
        response = question()
        print('    Question %-10s:\t%s' % (question.__name__, str(response)))
