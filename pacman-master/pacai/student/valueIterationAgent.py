from pacai.agents.learning.value import ValueEstimationAgent
from pacai.util import counter
from pacai.core.actions import Actions

class ValueIterationAgent(ValueEstimationAgent):
    """
    A value iteration agent.

    Make sure to read `pacai.agents.learning` before working on this class.

    A `ValueIterationAgent` takes a `pacai.core.mdp.MarkovDecisionProcess` on initialization,
    and runs value iteration for a given number of iterations using the supplied discount factor.

    Some useful mdp methods you will use:
    `pacai.core.mdp.MarkovDecisionProcess.getStates`,
    `pacai.core.mdp.MarkovDecisionProcess.getPossibleActions`,
    `pacai.core.mdp.MarkovDecisionProcess.getTransitionStatesAndProbs`,
    `pacai.core.mdp.MarkovDecisionProcess.getReward`.

    Additional methods to implement:

    `pacai.agents.learning.value.ValueEstimationAgent.getQValue`:
    The q-value of the state action pair (after the indicated number of value iteration passes).
    Note that value iteration does not necessarily create this quantity,
    and you may have to derive it on the fly.

    `pacai.agents.learning.value.ValueEstimationAgent.getPolicy`:
    The policy is the best action in the given state
    according to the values computed by value iteration.
    You may break ties any way you see fit.
    Note that if there are no legal actions, which is the case at the terminal state,
    you should return None.
    """

    def __init__(self, index, mdp, discountRate = 0.9, iters = 100, **kwargs):
        super().__init__(index, **kwargs)
        self.mdp = mdp
        self.discountRate = discountRate
        self.iters = iters
        # Compute the values here.
        self.values = counter.Counter()
        # current values will be a copy of values
        currentValues = counter.Counter()
        for i in range(self.iters):
            currentValues = self.values.copy()
            for state in self.mdp.getStates():
                actions = self.mdp.getPossibleActions(state)
                transitions = []
                valueList = []
                # for terminal states, we will just exit the game
                if self.mdp.isTerminal(state):
                    self.values[state] = 0
                else:
                    # for each action in each state for each iteration:
                    for action in actions:
                        # compute the initial q values
                        transitions = self.mdp.getTransitionStatesAndProbs(state, action)
                        value = 0
                        for transition, probability in transitions:
                            reward = self.mdp.getReward(state, action, transition)
                            discTrans = discountRate * currentValues[transition]
                            value += probability * (reward + discTrans)
                        valueList.append(value)
                    # the value of the state is the max of each q value
                    self.values[state] = max(valueList)

    def getValue(self, state):
        """
        Return the value of the state (computed in __init__).
        """
        return self.values[state]

    def getPolicy(self, state):
        """
        return an action based on what state is passed in.
        """
        valuesOfStates = []
        actions = self.mdp.getPossibleActions(state)
        # terminal states do not have a policy.
        if len(actions) == 0 or self.mdp.isTerminal(state):
            return None
        for action in actions:
            action = action.capitalize()
            if action != 'Exit':
                successor = Actions.getSuccessor(state, action)
                valuesOfStates.append(self.values[successor])

        if len(valuesOfStates) > 0:
            return self.mdp.getPossibleActions(state)[int(valuesOfStates.
                index(max(valuesOfStates)))]
        # if there are no possible actions, exit the game.
        return 'exit'

    def getQValue(self, state, action):
        """
        Computes the Q-value based on the formula given in the slides and textbook
        """
        value = 0
        for transition, probability in self.mdp.getTransitionStatesAndProbs(state, action):
            value += probability * (self.mdp.getReward(state, action, transition)
                  + (self.discountRate * self.values[transition]))
        return value

    def getAction(self, state):
        """
        Returns the policy at the state (no exploration).
        """
        return self.getPolicy(state)
