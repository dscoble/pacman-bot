"""
In this file, you will implement generic search algorithms which are called by Pacman agents.
"""
from pacai.util.stack import Stack
from pacai.util.queue import Queue
from pacai.util.priorityQueue import PriorityQueue

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first [p 85].

    Your search algorithm needs to return a list of actions that reaches the goal.
    Make sure to implement a graph search algorithm [Fig. 3.7].

    To get started, you might want to try some of these simple commands to
    understand the search problem that is being passed in:
    """

    # *** Your Code Here ***
    fringe = Stack()
    visited = []
    fringe.push((problem.startingState(), [], 0))
    while fringe.isEmpty() is False:
        curState, curMove, curCost = fringe.pop()
        if curState not in visited:
            if problem.isGoal(curState):
                return curMove
            visited.append(curState)
            for state, action, cost in problem.successorStates(curState):
                fringe.push((state, curMove + [action], curCost))
    raise NotImplementedError()

def breadthFirstSearch(problem):
    """
    Search the shallowest nodes in the search tree first. [p 81]
    """
    # *** Your Code Here ***
    fringe = Queue()
    visited = []
    fringe.push((problem.startingState(), [], 0))
    while fringe.isEmpty() is False:
        curState, curMove, curCost = fringe.pop()
        if problem.isGoal(curState):
            return curMove
            break
        if curState not in visited:
            visited.append(curState)
            for state, action, cost in problem.successorStates(curState):
                fringe.push((state, curMove + [action], curCost))
    print('fringe has emptied')
    return curMove

def uniformCostSearch(problem):
    """
    Search the node of least total cost first.
    """
    # *** Your Code Here ***
    fringe = PriorityQueue()
    visited = []
    fringe.push((problem.startingState(), [], 0), 0)
    while fringe.isEmpty() is False:
        curState, curMove, curCost = fringe.pop()
        if problem.isGoal(curState):
            return curMove
            break
        if curState not in visited:
            visited.append(curState)
            if problem.isGoal(curState):
                return curMove
                break
            for state, action, cost in problem.successorStates(curState):
                fringe.push((state, curMove + [action], curCost), curCost)
    raise NotImplementedError()

def aStarSearch(problem, heuristic):
    """
    Search the node that has the lowest combined cost and heuristic first.
    """
    # *** Your Code Here ***
    fringe = PriorityQueue()
    visited = []
    fringe.push((problem.startingState(), [], 0), 0)
    while fringe.isEmpty() is False:
        curState, curMove, curCost = fringe.pop()
        if problem.isGoal(curState):
            # print(len(curMove))
            return curMove
            break
        if curState not in visited:
            visited.append(curState)
            if problem.isGoal(curState):
                # print(len(curMove))
                return curMove
                break
            for state, action, cost in problem.successorStates(curState):
                fringe.push((state, curMove + [action], curCost),
                    curCost + heuristic(state, problem))
    return curMove
